<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreUser;
use App\Services\ResponseService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Transformers\User\UserResource;
use App\Transformers\User\UserResourceCollection;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param StoreUser $request
     * @return UserResource|JsonResponse
     */
    public function store(StoreUser $request)
    {
        try {
            $user = $this->user->create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => $request->get('password')
            ]);
        } catch (\Throwable|\Exception $e) {
            return ResponseService::exception('users.store', null, $e);
        }

        return new UserResource($user, ['type' => 'store', 'route' => 'users.store']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            $token = $this
                ->user
                ->login($credentials);
        } catch (\Throwable|\Exception $e) {
            return ResponseService::exception('users.login',null,$e);
        }
        return response()->json(compact('token'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|JsonResponse|\Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        try {
            $this->user->logout($request->input('token'));
        } catch(\Throwable|\Exception $e) {
            return ResponseService::exception('user.logout', null, $e);
        }

        return response(['status' => true, 'mensagem' => 'Deslogado com sucesso'], 200);
    }
}
