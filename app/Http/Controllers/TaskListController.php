<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskList\StoreTaskList;
use App\Services\ResponseService;
use App\TaskList;
use App\Transformers\TaskList\TaskListResource;
use App\Transformers\TaskList\TaskListResourceCollection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskListController extends Controller
{
    /**
     * @var TaskList
     */
    private $tasklist;

    public function __construct(TaskList $tasklist)
    {
        $this->tasklist = $tasklist;
    }

    /**
     * Display a listing of the resource.
     *
     * @return TaskListResourceCollection
     */
    public function index()
    {
        return new TaskListResourceCollection($this->tasklist->index());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTaskList $request
     * @return TaskListResource|JsonResponse
     */
    public function store(StoreTaskList $request)
    {
        try {
            $data = $this->tasklist->create($request->all());
        } catch (\Throwable|\Exception $e) {
            return ResponseService::exception('tasklist.store', null, $e);
        }

        return new TaskListResource($data, ['type' => 'store', 'route' => 'tasklist.store']);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return TaskListResource|JsonResponse|Response
     */
    public function show($id)
    {
        try {
            $data = $this->tasklist->show($id);
        } catch (\Throwable|\Exception $e) {
            return ResponseService::exception('tasklist.show', $id, $e);
        }

        return new TaskListResource($data, ['type' => 'show', 'route' => 'tasklist.store']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return TaskListResource|JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $this->tasklist->updateList($request->all(), $id);
        } catch (\Throwable|\Exception $e) {
            return ResponseService::exception('tasklist.update', $id, $e);
        }

        return new TaskListResource($data, ['type' => 'update', 'route' => 'tasklist.update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return TaskListResource|JsonResponse
     */
    public function destroy($id)
    {
        try {
            $data = $this->tasklist->destroyList($id);
        } catch (\Throwable|\Exception $e) {
            return ResponseService::exception('tasklist.destroy', $id, $e);
        }

        return new TaskListResource($data, ['type' => 'destroy', 'route' => 'tasklist.destroy']);
    }
}
