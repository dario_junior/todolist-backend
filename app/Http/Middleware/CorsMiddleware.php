<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class CorsMiddleware
 * @package App\Http\Middleware
 */
class CorsMiddleware
{
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Origin', $this->originAllow())
            ->header('Access-Control-Allow-Methods', "PUT, POST, DELETE, GET, OPTIONS")
            ->header('Access-Control-Allow-Headers', "Accept, Authorization, Content-Type, x-requested-with");
    }

    private function originAllow()
    {
        $origin = '*';
//        $origin = $_SERVER['HTTP_ORIGIN'];
        return $origin;
    }
}
