<?php

namespace App\Services;

use Illuminate\Http\JsonResponse;

/**
 * Class ResponseService
 * @package App\Services
 */
class ResponseService
{
    /**
     * Default Responses.
     *
     * @param array $config
     * @param null $id
     * @return array
     */
    public static function default($config = array(), $id = null)
    {
        $route = $config['route'];
        switch ($config['type']) {
            case 'store':
                return [
                    'status' => true,
                    'message' => 'Dado inserido com sucesso',
                    'url' => route($route)
                ];
                break;
            case 'show':
                return [
                    'status' => true,
                    'message' => 'Requisição realizada com sucesso',
                    'url' => $id != null ? route($route, $id) : route($route)
                ];
                break;
            case 'update':
                return [
                    'status' => true,
                    'message' => 'Dados Atualizado com sucesso',
                    'url' => $id != null ? route($route, $id) : route($route)
                ];
                break;
            case 'destroy':
                return [
                    'status' => true,
                    'message' => 'Dado excluido com sucesso',
                    'url' => $id != null ? route($route, $id) : route($route)
                ];
                break;
        }
    }

    /**
     * Register services.
     *
     * @param $route
     * @param null $id
     * @param $e
     * @return JsonResponse
     */
    public static function exception($route, $id = null, $e)
    {
        switch ($e->getCode()) {
            case -403:
                return response()->json([
                    'status' => false,
                    'statusCode' => 403,
                    'error' => $e->getMessage(),
                    'url' => $id != null ? route($route, $id) : route($route)
                ], 403);
            case -404:
                return response()->json([
                    'status' => false,
                    'statusCode' => 404,
                    'error' => $e->getMessage(),
                    'url' => $id != null ? route($route, $id) : route($route)
                ], 404);
            default:
                if (app()->bound('sentry')) {
                    $sentry = app('sentry');
                    $user = auth()->user();
                    if ($user) {
                        $sentry->user_context(['id' => $user->id, 'name' => $user->name]);
                    }
                    $sentry->captureException($e);
                }
                return response()->json([
                    'status' => false,
                    'statusCode' => 500,
                    'error' => 'Problema ao realizar a operação.',
                    'url' => $id != null ? route($route, $id) : route($route)
                ], 500);
        }
    }
}