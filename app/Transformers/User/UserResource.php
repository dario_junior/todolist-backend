<?php

namespace App\Transformers\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use App\Services\ResponseService;
use Illuminate\Http\Response;

class UserResource extends Resource
{
    /**
     * Create a new resource instance.
     *
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return ['data' => $this->resource];
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            'status' => true,
            'mensaggem'    => 'Dado inserido com sucesso.',
            'url'    => route('users.store')
        ];
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param  Request
     * @param  Response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $response->setStatusCode(200);
    }
}