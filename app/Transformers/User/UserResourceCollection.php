<?php

namespace App\Transformers\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Services\ResponseService;
use Illuminate\Http\Response;

class UserResourceCollection extends ResourceCollection
{
    /**
     * Create a new resource instance.
     *
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return ['data' => $this->collection];
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param Request $request
     * @return array
     */
    public function with($request)
    {
        return [
            'status' => true,
            'msg' => 'Listando dados',
            'url' => route('users.index')
        ];
    }

    /**
     * Customize the outgoing response for the resource.
     *
     * @param Request
     * @param Response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $response->setStatusCode(200);
    }
}