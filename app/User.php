<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function create(array $fields)
    {
        return parent::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'password' => Hash::make($fields['password']),
        ]);
    }

    /**
     * @param $credentials
     * @return false|string
     * @throws Exception
     */
    public function login($credentials)
    {
        if (!$token = JWTAuth::attempt($credentials)) {
            throw new Exception("Credencias incorretas, verifique-as e tente novamente.", -404);
        }
        return $token;
    }

    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @param $token
     * @throws Exception
     */
    public function logout($token)
    {
        if (!JWTAuth::invalidate($token)) {
            throw new Exception('Erro. Tente novamente', -404);
        }
    }

    /**
     * @return HasMany
     */
    public function tasklist()
    {
        return $this->hasMany('App\TaskList');
    }

    /**
     * @return HasMany
     */
    public function tasks()
    {
        return $this->hasMany('App\Tasks');
    }
}
